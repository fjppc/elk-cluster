# elk-cluster

#### 介绍
Elk集群 + APM

docker-compose.yml包含ELK集群

docker-compose-apm.yml包含ELK集群跟APM

#### 安装教程

1. 安装docker-compose
    ```
    // 下载
    curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    
    // 赋予权限
    chmod +x /usr/local/bin/docker-compose
    ```

2. 修改sysctl文件
    ```
    vim /etc/sysctl.conf
    
    // 新增内容
    vm.max_map_count=1024000
    
    // 使配置生效
    sysctl -p
    ```

3. 修改文件夹权限
    ```
    cd /home/xxx/elk-cluster

    for node in `seq 1 3`; do \
    mkdir -p ./es0${node}/data; mkdir -p ./es0${node}/logs; \
    done

    mkdir -p ./kibana/logs;

    mkdir -p ./logstash/data;

    mkdir -p ./logstash/logs;

    chown 1000:1000 /home/xxx/elk-cluster
    
    chown 1000:1000 /home/xxx/elk-cluster/*
    
    chown 1000:1000 /home/xxx/elk-cluster/*/*

    chown 1000:1000 /home/xxx/elk-cluster/*/*/*
    ``` 

4. 运行docker-compose
    ```
    docker-compose -f xxx/elk-cluster/docker-compose.yml -d
    
    // docker-compose -f xxx/elk-cluster/docker-compose-apm.yml -d
    ```
